<?php

/**
 * @file
 * Contains \Drupal\hybridauth\Plugin\Block\HybridauthIconsBlock.
 */

namespace Drupal\hybridauth\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

/**
 * Provides a 'HybridauthIconsBlock' block.
 *
 * @Block(
 *  id = "hybridauth_icons_block",
 *  admin_label = @Translation("Hybridauth icons block"),
 * )
 */
class HybridauthIconsBlock extends BlockBase {


  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $config = \Drupal::config('hybridauth.settings');
    $values = $config->get('hybridauth_settings');
    $enabled_providers = array_filter($values['hybridauth_providers']);
    foreach ($enabled_providers as $provider) {
      $p_path = '/hybridauth/window/' . $provider;
      $url = Url::fromUserInput($p_path);
      $external_link = \Drupal::l(t($provider), $url);
      $build['hybridauth_icons_block' . $provider]['#markup'] = $external_link;
    }

    return $build;
  }

}
