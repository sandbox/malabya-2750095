<?php

/**
 * @file
 * Contains \Drupal\hybridauth\Form\HybridauthAdminSettings.
 */

namespace Drupal\hybridauth\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;

class HybridauthAdminSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hybridauth_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('hybridauth.settings');
    $config->set('hybridauth_settings', $form_state->getValues());
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'hybridauth.settings',
    ];
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('hybridauth.settings');
    $values = $config->get('hybridauth_settings');

    if ($config->get('hybridauth_register') == 0 && $config->get('user_register') == USER_REGISTER_ADMINISTRATORS_ONLY) {
      drupal_set_message(t('Currently only administrators can create new user accounts. Either change the "Who can register accounts?" option under "Account settings" tab or at the core <a href="!link">Account settings</a>.', [
        '!link' => Url::fromUri('admin/config/people/accounts')
        ]), 'warning');
    }

    $form['vtabs'] = array(
      '#type' => 'vertical_tabs',
      '#title' => t('Hybrid Auth Settings'),
    );
    $form['fset_providers'] = array(
      '#type' => 'details',
      '#title' => t('Authentication providers'),
      '#group' => 'vtabs',
    );
    $header = [
      'name' => t('Name'),
      // 'icon' => t('Icon'),
      'available' => t('Available'),
      'settings' => t('Settings'),
    ];
    $options = [];
    $weight = -50;
    // Clear the providers cache here to get any new ones.
    $providers = hybridauth_providers_list(TRUE);
    $enabled_providers = array_filter($values['hybridauth_providers']);
    $available_providers = hybridauth_providers_files();
    //$form['fset_providers']['hybridauth_providers'] = [];
    // $icon_pack = variable_get('hybridauth_widget_icon_pack', 'hybridauth_32');
    $icon_pack = 'hybridauth_16';
    _hybridauth_add_icon_pack_files($icon_pack, $form);
    // @todo currently remove enabled providers, should be opened again when done.
    // foreach (array_keys($enabled_providers + $providers) as $provider_id) {
    if (!empty($providers)) {
      foreach (array_keys($providers) as $provider_id) {
        $available = array_key_exists($provider_id, $available_providers);

        $session_url = Url::fromUserInput('/admin/config/people/hybridauth/provider/' . $provider_id);
        $options[$provider_id] = [
          'name' => $providers[$provider_id],
          // @todo - Need to be checked.
          /*'icon' => theme('hybridauth_provider_icon', [
            'icon_pack' => $icon_pack,
            'provider_id' => $provider_id,
            'provider_name' => $providers[$provider_id],
          ]),*/
          'available' => $available ? t('Yes') : t('No'),
          'settings' => Link::fromTextAndUrl(t('Settings'), $session_url),
          // '#attributes' => ['class' => ['draggable']],
        ];
        $form['fset_providers']['hybridauth_providers']['hybridauth_provider_' . $provider_id . '_weight'] = [
          '#tree' => FALSE,
          '#type' => 'weight',
          '#delta' => 50,
          '#default_value' => $weight++,
          '#attributes' => [
            'class' => [
              'hybridauth-providers-weight'
            ]
          ],
        ];
      }
    }
    $a = '';
//    $form['fset_providers']['hybridauth_providers'] = [
//      '#type' => 'tableselect',
//      '#title' => t('Providers'),
//      '#header' => $header,
//      '#options' => '', $options,
//      '#default_value' => '', //$enabled_providers,
//      /*'#pre_render' => [
//        'hybridauth_admin_providers_pre_render'
//      ],*/
//    ];
    $form['fset_providers']['hybridauth_providers'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No users found'),
      '#default_value' => $enabled_providers,
      '#attributes' => array(
        'class' => array('draggable'),
      ),
    ];


    $a = '';
    /*   */
    $form['fset_fields'] = array(
      '#type' => 'details',
      '#title' => t('Required information'),
      '#group' => 'vtabs',
    );
    /*$form['vtabs']['fset_fields'] = [
      '#type' => 'fieldset',
      '#title' => t('Required information'),
    ];*/
    $form['fset_fields']['hybridauth_required_fields'] = [
      '#type' => 'checkboxes',
      '#title' => t('Required information'),
      '#options' => [
        'email' => t('Email address'),
        'firstName' => t('First name'),
        'lastName' => t('Last name'),
        'gender' => t('Gender'),
      ],
      '#description' => t("If authentication provider doesn't return it, visitor will need to fill additional form before registration."),
      '#default_value' => $values['hybridauth_required_fields'],
    ];

    $a = '';
    /*    */
    $form['fset_widget'] = array(
      '#type' => 'details',
      '#title' => t('Widget settings'),
      '#group' => 'vtabs',
    );
    /*$form['vtabs']['fset_widget'] = [
      '#type' => 'fieldset',
      '#title' => t('Widget settings'),
    ];*/
    $form['fset_widget']['hybridauth_widget_title'] = [
      '#type' => 'textfield',
      '#title' => t('Widget title'),
      '#default_value' => $values['hybridauth_widget_title'],
    ];
    $options = [
      'list' => t('Enabled providers icons'),
      'button' => t('Single icon leading to a list of enabled providers'),
      'link' => t('Link leading to a list of enabled providers'),
    ];
    $form['fset_widget']['hybridauth_widget_type'] = [
      '#type' => 'radios',
      '#title' => t('Widget type'),
      '#options' => $options,
      '#default_value' => $values['hybridauth_widget_type'],
    ];
    $form['fset_widget']['hybridauth_widget_use_overlay'] = [
      '#type' => 'checkbox',
      '#title' => t('Display list of enabled providers using overlay'),
      '#default_value' => $values['hybridauth_widget_use_overlay'],
      '#states' => [
        'invisible' => [
          ':input[name="hybridauth_widget_type"]' => [
            'value' => 'list'
          ]
        ]
      ],
    ];
    $form['fset_widget']['hybridauth_widget_link_text'] = [
      '#type' => 'textfield',
      '#title' => t('Link text'),
      '#default_value' => $values['hybridauth_widget_link_text'],
      '#states' => [
        'visible' => [
          ':input[name="hybridauth_widget_type"]' => [
            'value' => 'link'
          ]
        ]
      ],
    ];
    $form['fset_widget']['hybridauth_widget_link_title'] = [
      '#type' => 'textfield',
      '#title' => t('Link or icon title'),
      '#default_value' => $values['hybridauth_widget_link_title'],
      '#states' => [
        'invisible' => [
          ':input[name="hybridauth_widget_type"]' => [
            'value' => 'list'
          ]
        ]
      ],
    ];
    $options = [];
    foreach (hybridauth_get_icon_packs() as $name => $icon_pack) {
      $options[$name] = $icon_pack['title'];
    }
    $form['fset_widget']['hybridauth_widget_icon_pack'] = [
      '#type' => 'select',
      '#title' => t('Icon package'),
      '#options' => $options,
      '#default_value' => $values['hybridauth_widget_icon_pack'],
    ];
    $form['fset_widget']['hybridauth_widget_weight'] = [
      '#type' => 'weight',
      '#title' => t('Widget weight'),
      '#delta' => 200,
      '#description' => t('Determines the order of the elements on the form - heavier elements get positioned later.'),
      '#default_value' => $values['hybridauth_widget_weight'],
    ];
    $form['fset_widget']['hybridauth_widget_hide_links'] = [
      '#type' => 'checkbox',
      '#title' => t('Hide links'),
      '#description' => t('Remove href property from authentication provider links.'),
      '#default_value' => $values['hybridauth_widget_hide_links'],
    ];
    $a = '';



    /*    */
    $form['fset_account'] = array(
      '#type' => 'details',
      '#title' => t('Account settings'),
      '#group' => 'vtabs',
    );
    /*$form['vtabs']['fset_account'] = [
      '#type' => 'fieldset',
      '#title' => t('Account settings'),
    ];*/

    $core_settings = [
      USER_REGISTER_ADMINISTRATORS_ONLY => t('Administrators only'),
      USER_REGISTER_VISITORS => t('Visitors'),
      USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL => t('Visitors, but administrator approval is required'),
    ];
    $a = '';
    $form['fset_account']['hybridauth_register'] = [
      '#type' => 'radios',
      '#title' => t('Who can register accounts?'),
      '#options' => [
        // 0 => t('Follow core') . ': ' . $core_settings[$config->get('user_register')],
        1 => t('Visitors'),
        2 => t('Visitors, but administrator approval is required'),
        3 => t('Nobody, only login for existing accounts is possible'),
      ],
      /*'#description' => t('Select who can register accounts through HybridAuth.') . ' ' . t('The core settings are available at <a href="!link">Account settings</a>.', [
          '!link' => Url::fromUri('admin/config/people/accounts')
        ]),*/
      '#default_value' => $values['hybridauth_register'],
    ];
    $a = '';
    $core_settings = [
      0 => t("Don't require e-mail verification"),
      1 => t('Require e-mail verification'),
    ];
    $form['fset_account']['hybridauth_email_verification'] = [
      '#type' => 'radios',
      '#title' => t('E-mail verification'),
      '#options' => [
        0 => t('Follow core') . ': ' . $core_settings[$config->get('user_email_verification')],
        1 => t('Require e-mail verification'),
        2 => t("Don't require e-mail verification"),
      ],
      /*'#description' => t("Select how to handle not verified e-mail addresses (authentication provider gives non-verified e-mail address or doesn't provide one at all).") . ' ' . t('The core settings are available at <a href="!link">Account settings</a>.', [
          '!link' => Url::fromUri('admin/config/people/accounts')
        ]),*/
      '#default_value' => $values['hybridauth_email_verification'],
    ];
    $a = '';
    // E-mail address verification template.
    $form['fset_account']['fset_email_verification_template'] = [
      '#type' => 'fieldset',
      '#title' => t('E-mail verification template'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#states' => [
        'invisible' => [
          ':input[name="hybridauth_email_verification"]' => [
            'value' => '2'
          ]
        ]
      ],
    ];
    $form['fset_account']['fset_email_verification_template']['hybridauth_email_verification_subject'] = [
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#default_value' => _hybridauth_mail_text('hybridauth_email_verification_subject', NULL, [], FALSE),
      '#maxlength' => 180,
    ];
    $form['fset_account']['fset_email_verification_template']['hybridauth_email_verification_body'] = [
      '#type' => 'textarea',
      '#title' => t('Body'),
      '#default_value' => _hybridauth_mail_text('hybridauth_email_verification_body', NULL, [], FALSE),
      '#rows' => 12,
    ];
    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $form['fset_account']['fset_email_verification_template']['hybridauth_token_tree'] = [
        '#theme' => 'token_tree',
        '#token_types' => [
          'user'
        ],
        '#global_types' => TRUE,
        '#dialog' => TRUE,
      ];
    }
    $form['fset_account']['hybridauth_pictures'] = [
      '#type' => 'checkbox',
      '#title' => t('Save HybridAuth provided picture as user picture'),
      '#description' => t('Save pictures provided by HybridAuth as user pictures. Check the "Enable user pictures" option at <a href="@link">Account settings</a> to make this option available.',
        ['@link' => '/admin/config/people/accounts']),
      '#default_value' => $values['hybridauth_pictures'],
      // '#disabled' => $config->get('user_pictures'),
    ];
    $form['fset_account']['hybridauth_username'] = [
      '#type' => 'textfield',
      '#title' => t('Username pattern'),
      '#default_value' => $values['hybridauth_username'],
      /*'#description' => t('Create username for new users using this pattern; counter will be added in case of username conflict.') . ' ' . t('You should use only HybridAuth tokens here as the user is not created yet.') . ' ' . t('Install !link module to get list of all available tokens.', [
        '!link' => \Drupal::l(t('Token'), 'http://drupal.org/project/token', [
          'attributes' => [
            'target' => '_blank'
            ]
          ])
        ]),*/
      '#required' => TRUE,
    ];
    $form['fset_account']['hybridauth_display_name'] = [
      '#type' => 'textfield',
      '#title' => t('Display name pattern'),
      '#default_value' => $values['hybridauth_display_name'],
      /*'#description' => t('Leave empty to not alter display name. You can use any user tokens here.') . ' ' . t('Install !link module to get list of all available tokens.', [
        '!link' => \Drupal::l(t('Token'), 'http://drupal.org/project/token', [
          'attributes' => [
            'target' => '_blank'
            ]
          ])
        ]),*/
    ];
    $a = '';
    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $form['fset_account']['fset_token'] = [
        '#type' => 'fieldset',
        '#title' => t('Tokens'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      ];
      $form['fset_account']['fset_token']['hybridauth_token_tree'] = [
        '#theme' => 'token_tree',
        '#token_types' => [
          'user'
          ],
        '#global_types' => FALSE,
        '#dialog' => TRUE,
      ];
    }
    $form['fset_account']['hybridauth_registration_username_change'] = [
      '#type' => 'checkbox',
      '#title' => t('Allow username change when registering'),
      '#description' => t('Allow users to change their username when registering through HybridAuth.'),
      '#default_value' => $values['hybridauth_registration_username_change'],
    ];
    $form['fset_account']['hybridauth_registration_password'] = [
      '#type' => 'checkbox',
      '#title' => t('Ask user for a password when registering'),
      '#description' => t('Ask users to set password for account when registering through HybridAuth.'),
      '#default_value' => $values['hybridauth_registration_password'],
    ];
    $a = '';
    $form['fset_account']['hybridauth_override_realname'] = [
      '#type' => 'checkbox',
      '#title' => t('Override Real name'),
      /*'#description' => t('Override <a href="!link1">Real name settings</a> using the above display name pattern for users created by HybridAuth. This option is available only if !link2 module is installed.', [
        '!link1' => Url::fromUri('admin/config/people/realname'),
        '!link2' => \Drupal::l(t('Real name'), 'http://drupal.org/project/realname', [
          'attributes' => [
            'target' => '_blank'
            ]
          ]),
      ]),*/
      '#default_value' => $values['hybridauth_override_realname'],
      '#disabled' => !\Drupal::moduleHandler()->moduleExists('realname'),
    ];
    $form['fset_account']['hybridauth_disable_username_change'] = [
      '#type' => 'checkbox',
      '#title' => t('Disable username change'),
      '#description' => t('Remove username field from user account edit form for users created by HybridAuth.' . ' If this is unchecked then users should also have "Change own username" permission to actually be able to change the username.'),
      '#default_value' => $values['hybridauth_disable_username_change'],
    ];
    $form['fset_account']['hybridauth_remove_password_fields'] = [
      '#type' => 'checkbox',
      '#title' => t('Remove password fields'),
      '#description' => t('Remove password fields from user account edit form for users created by HybridAuth.'),
      '#default_value' => $values['hybridauth_disable_username_change'],
    ];



    /*    */
    $form['fset_forms'] = array(
      '#type' => 'details',
      '#title' => t('Drupal forms'),
      '#group' => 'vtabs',
    );
    /*$form['vtabs']['fset_forms'] = [
      '#type' => 'fieldset',
      '#title' => t('Drupal forms'),
    ];*/
    $a = '';
    $form['fset_forms']['hybridauth_forms'] = [
      '#type' => 'checkboxes',
      '#title' => t('Drupal forms'),
      '#options' => hybridauth_forms_list(),
      '#description' => t('Add HybridAuth widget to these forms.'),
      '#default_value' => $values['hybridauth_forms'],
    ];


    /*    */
    $form['fset_other'] = array(
      '#type' => 'details',
      '#title' => t('Other settings'),
      '#group' => 'vtabs',
    );
    /*$form['fset_other'] = [
      '#type' => 'fieldset',
      '#title' => t('Other settings'),
    ];*/
    /*$destination_description = t('Drupal path to redirect to, like "node/1". Leave empty to return to the same page (set to [HTTP_REFERER] for widget in modal dialogs loaded by AJAX).') . '<br/>' . t('You can use any user or global tokens here.') . ' ' . t('Install !link module to get list of all available tokens.', [
      '!link' => \Drupal::l(t('Token'), 'http://drupal.org/project/token', [
        'attributes' => [
          'target' => '_blank'
          ]
        ])
      ]);*/
    $form['fset_other']['hybridauth_destination'] = [
      '#type' => 'textfield',
      '#title' => t('Redirect after login'),
      '#default_value' => $values['hybridauth_destination'],
      //'#description' => $destination_description,
    ];
    $form['fset_other']['hybridauth_destination_error'] = [
      '#type' => 'textfield',
      '#title' => t('Redirect after error on login'),
      '#default_value' => $values['hybridauth_destination_error'],
      //'#description' => $destination_description,
    ];
    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $form['fset_other']['fset_token'] = [
        '#type' => 'fieldset',
        '#title' => t('Tokens'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      ];
      $form['fset_other']['fset_token']['hybridauth_token_tree'] = [
        '#theme' => 'token_tree',
        '#token_types' => [
          'user'
          ],
        '#global_types' => TRUE,
        '#dialog' => TRUE,
      ];
    }
    $options = [
      0 => t('Allow duplicate email addresses, create new user account and login'),
      1 => t("Don't allow duplicate email addresses, block registration and advise to login using the existing account"),
      2 => t("Don't allow duplicate email addresses, add new identity to the existing account and login"),
    ];
    $a = '';
    $form['fset_other']['hybridauth_duplicate_emails'] = [
      '#type' => 'radios',
      '#title' => t('Duplicate emails'),
      '#options' => $options,
      '#default_value' => $values['hybridauth_duplicate_emails'],
//      '#description' => t('Select how to handle duplicate email addresses. ' . 'This situation occurs when the same user is trying to authenticate using different authentication providers, but with the same email address.'),
    ];
    $form['fset_other']['hybridauth_proxy'] = [
      '#type' => 'textfield',
      '#title' => t('Proxy'),
      '#default_value' => $values['hybridauth_proxy'],
//      '#description' => t('The HTTP proxy to tunnel requests through. For example <strong>1.2.3.4:8080</strong>.'),
    ];
    $form['fset_other']['hybridauth_debug'] = [
      '#type' => 'checkbox',
      '#title' => t('Debug mode'),
//      '#description' => t('When in debug mode, extra error information will be logged/displayed. This should be disabled when not in development.'),
      '#default_value' => $values['hybridauth_debug'],
    ];

    return parent::buildForm($form, $form_state);
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    /*$values = &$form_state->getValues();
    $providers = hybridauth_providers_list();

    // Remove weights so they are not saved as variables.
    foreach (array_keys($providers) as $provider_id) {
      $providers_weights[$provider_id] = $values['hybridauth_provider_' . $provider_id . '_weight'];
      unset($values['hybridauth_provider_' . $provider_id . '_weight']);
    }
    asort($providers_weights);

    $providers_values = $values['hybridauth_providers'];
    unset($values['hybridauth_providers']);

    foreach (array_keys($providers_weights) as $provider_id) {
      $values['hybridauth_providers'][$provider_id] = $providers_values[$provider_id];
    }*/
    parent::validateForm($form, $form_state);
  }

}
