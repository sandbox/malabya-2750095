<?php

/**
 * @file
 * Contains \Drupal\hybridauth\Form\HybridauthUserIdentityDelete.
 */

namespace Drupal\hybridauth\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class HybridauthUserIdentityDelete extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hybridauth_user_identity_delete';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $account = NULL, $id = NULL) {
    $del_identity = _hybridauth_identity_load_by_id($id);
    if (!$del_identity || $account->uid != $del_identity['uid']) {
      drupal_set_message(t('You are trying to delete non-existing identity.'), 'error');
      drupal_access_denied();
      // See https://drupal.org/node/2020351 and
      // https://api.drupal.org/api/drupal/includes%21common.inc/function/drupal_access_denied/7
      drupal_exit();
    }
    $del_identity_data = unserialize($del_identity['data']);
    $username = format_username($account);
    $question = t('Are you sure you want to detach the HybridAuth identity !identity from %user?', [
      '!identity' => l($del_identity_data['profileURL'], $del_identity_data['profileURL'], [
        'attributes' => [
          'target' => '_blank'
          ],
        'external' => TRUE,
      ]),
      '%user' => $username,
    ]);

    $form = [];
    $form['#user'] = $account;
    $form['#del_identity'] = $del_identity;
    $form['#del_identity_data'] = $del_identity_data;
    $form['question'] = [
      '#markup' => $question,
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    ];

    if (!empty($account->data['hybridauth']) && $account->data['hybridauth']['provider'] == $del_identity_data['provider'] && $account->data['hybridauth']['identifier'] == $del_identity_data['identifier']) {
      $identities = _hybridauth_identity_load_by_uid($account->uid);
      $providers = hybridauth_providers_list();
      $options = [];
      foreach ($identities as $key => $identity) {
        $data = unserialize($identity['data']);
        if ($key != $id) {
          $options[$key] = $providers[$identity['provider']] . ' - ' . l($data['profileURL'], $data['profileURL'], [
            'attributes' => [
              'target' => '_blank'
              ],
            'external' => TRUE,
          ]);
        }
      }

      if (!empty($options)) {
        $form['explanation'] = [
          '#markup' => t('This identity was used to create your account. Please choose another identity to replace it.'),
          '#prefix' => '<div>',
          '#suffix' => '</div>',
        ];
        $form['identity_choice'] = [
          '#type' => 'radios',
          // '#title' => t('Identities'),
        '#options' => $options,
          '#default_value' => count($options) == 1 ? $key : NULL,
          // '#required' => TRUE, //required has bugs with radios http://drupal.org/node/811542.
        ];
      }
      else {
        $form['explanation'] = [
          '#markup' => t('This identity was used to create your account. To delete it you should first add another identity to your account.'),
          '#prefix' => '<div>',
          '#suffix' => '</div>',
        ];
        // Add more identities.
        if (user_access('use hybridauth')) {
          $form['hybridauth_widget'] = [
            '#type' => 'hybridauth_widget',
            '#title' => t('Add more identities'),
            '#weight' => 10,
            '#hybridauth_widget_type' => 'list',
            '#hybridauth_destination' => '',
            '#hybridauth_destination_error' => '',
          ];
        }
        return $form;
      }
    }

    $form = confirm_form($form, '', 'user/' . $account->uid . '/hybridauth');
    drupal_set_title($username);

    return $form;
  }

  public function validateForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    if (!empty($form['identity_choice']) && !$form_state->getValue([
      'identity_choice'
      ])) {
      $form_state->setErrorByName('identity_choice', t('Please choose identity for replacement.'));
    }
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $account = $form['#user'];
    $del_identity = $form['#del_identity'];
    $del_identity_data = $form['#del_identity_data'];
    if ($form_state->getValue(['identity_choice'])) {
      // Change hybridauth data used for tokens.
      $identity = _hybridauth_identity_load_by_id($form_state->getValue(['identity_choice']));
      $data = unserialize($identity['data']);
      $edit['data']['hybridauth'] = $data;
      // Change name.
      // $name = _hybridauth_make_username($data);
      // $edit['name'] = $name;
      $account = user_save($account, $edit);
    }

    $deleted = _hybridauth_identity_delete_by_id($del_identity['id']);
    if ($deleted) {
      drupal_set_message(t('Identity deleted.'));
      _hybridauth_invoke_hooks('hybridauth_identity_deleted', $account, $del_identity_data);
    }

    if ($hybridauth = hybridauth_get_instance()) {
      if (is_object($hybridauth)) {
        $adapter = $hybridauth->getAdapter($del_identity['provider']);
        $adapter->logout();
      }
    }

    $form_state->set(['redirect'], 'user/' . $account->uid . '/hybridauth');
  }

}
