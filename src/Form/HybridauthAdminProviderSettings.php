<?php

/**
 * @file
 * Contains \Drupal\hybridauth\Form\HybridauthAdminProviderSettings.
 */

namespace Drupal\hybridauth\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class HybridauthAdminProviderSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hybridauth_admin_provider_settings';
  }
  
  public function hybridauth_get_provider_name($provider_id = NULL){
    return $provider_id.' Configuration';
  }
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['hybridauth.settings'];
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $provider_id = NULL) {
    $config = $this->config('hybridauth.settings');
    $form['vtabs'] = ['#type' => 'vertical_tabs'];

    $form['vtabs']['application'] = [
      '#type' => 'fieldset',
      '#title' => t('Application consumer key'),
      '#description' => t('The Application consumer key.'),
    ];
    $form['application']['hybridauth_provider_' . $provider_id . '_keys_key'] = [
      '#type' => 'textfield',
      '#title' => t('Client Key'),
      '#description' => t('The Client key.'),
      '#default_value' => $config->get('hybridauth_provider_' . $provider_id . '_keys_key'),
    ]; 
      $form['application']['hybridauth_provider_' . $provider_id . '_id'] = [
      '#type' => 'textfield',
      '#title' => t('Client id'),
      '#description' => t('The Client id.'),
      '#default_value' => $config->get('hybridauth_provider_' . $provider_id . '_id'),
    ]; 
    $form['application']['hybridauth_provider_' . $provider_id . '_keys_secret'] = [
      '#type' => 'textfield',
      '#title' => t('Client Secret'),
      '#description' => t('The Client Secret.'),
      '#default_value' => $config->get('hybridauth_provider_' . $provider_id . '_keys_secret'),
    ];/*
      $form['application']['hybridauth_provider_' . $provider_id . '_keys_secret'] = [
      '#type' => 'textfield',
      '#title' => t('Application consumer secret'),
      '#description' => t('The application consumer secret key.'),
      //  '#default_value' => variable_get('hybridauth_provider_' . $provider_id . '_keys_secret', ''),
      ];

      $form['window_settings'] = [
      '#type' => 'fieldset',
      '#title' => t('Authentication window settings'),
      ];
      $options = [
      'current' => t('Current window'),
      'popup' => t('New popup window'),
      ];
      /*$modal_description = FALSE;
      if (module_exists('colorbox')) {
      $options['colorbox'] = t('Colorbox');
      $modal_description = TRUE;
      }
      if (module_exists('shadowbox')) {
      $options['shadowbox'] = t('Shadowbox');
      $modal_description = TRUE;
      }
      if (module_exists('fancybox')) {
      $options['fancybox'] = t('fancyBox');
      $modal_description = TRUE;
      }
      if (module_exists('lightbox2')) {
      $options['lightbox2'] = t('Lightbox2');
      $modal_description = TRUE;
      }
      @todo: Integrate with ctool plugin

      $form['vtabs']['window_settings']['hybridauth_provider_' . $provider_id . '_window_type'] = [
      '#type' => 'radios',
      '#title' => t('Authentication window type'),
      '#options' => $options,
      //'#default_value' => variable_get('hybridauth_provider_' . $provider_id . '_window_type', 'current'),
      '#description' => $modal_description ? t("Be careful with modal windows - some authentication providers (Twitter, LinkedIn) won't work with them.") : '',
      ];
      $base = [
      '#type' => 'textfield',
      '#element_validate' => [
      'element_validate_integer_positive'
      ],
      '#size' => 4,
      '#maxlength' => 4,
      '#states' => [
      'invisible' => [
      ':input[name="hybridauth_provider_' . $provider_id . '_window_type"]' => [
      'value' => 'current'
      ]
      ]
      ],
      ];
      $form['vtabs']['window_settings']['hybridauth_provider_' . $provider_id . '_window_width'] = [
      '#title' => t('Width'),
      '#description' => t('Authentication window width (pixels).'),
      //'#default_value' => variable_get('hybridauth_provider_' . $provider_id . '_window_width', 800),
      ] + $base;
      $form['vtabs']['window_settings']['hybridauth_provider_' . $provider_id . '_window_height'] = [
      '#title' => t('Height'),
      '#description' => t('Authentication window height (pixels).'),
      // '#default_value' => variable_get('hybridauth_provider_' . $provider_id . '_window_height', 500),
      ] + $base;

      if ($provider = hybridauth_get_provider($provider_id)) {
      if ($function = ctools_plugin_get_function($provider, 'configuration_form_callback')) {
      $function($form, $provider_id);
      }
      }

     */

    return parent::buildForm($form, $form_state);
  }

  public function validateForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('hybridauth.settings');
    foreach ($form_state->getValues() as $variable_name => $value) {
      if (strpos($variable_name, 'hybridauth_provider_') === 0) {
        $config->set($variable_name, $form_state->getValues()[$variable_name]);
        $config->save();
      }
    }
    parent::submitForm($form, $form_state);
  }

}
