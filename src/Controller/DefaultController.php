<?php

/**
 * @file
 * Contains \Drupal\hybridauth\Controller\DefaultController.
 */

namespace Drupal\hybridauth\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Drupal\user\Entity\User;

/**
 * Default controller for the hybridauth module.
 */
class DefaultController extends ControllerBase {

  public function hybridauth_report() {
    $providers = hybridauth_providers_list();

    $header = [t('Authentication provider'), t('Users count')];
    $rows = [];
    $query = db_select('hybridauth_identity', 'ha_id');
    $query->addField('ha_id', 'provider', 'provider');
    $query->addExpression('COUNT(provider_identifier)', 'count');
    $query->groupBy('provider');
    $results = $query
        ->execute()
        ->fetchAllAssoc('provider', PDO::FETCH_ASSOC);
    foreach ($results as $result) {
      $rows[] = [
        $providers[$result['provider']],
        $result['count'],
      ];
    }

    $build = [];
    $build['report'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('There are no HybridAuth identities yet.'),
    ];
    return $build;
  }

  public function hybridauth_endpoint() {
    // Make sure the session is started, HybridAuth library needs it.
    // hybridauth_session_start();
     session_start();

    if ($lib_path = _hybridauth_library_path()) {
      try {
        // If Composer install was executed in the Hybridauth library use that
        // autoloader.
        if (file_exists($lib_path . '/../vendor/autoload.php')) {
          require_once $lib_path . '/../vendor/autoload.php';
        }
        require_once $lib_path . '/index.php';
      }
      catch (\Exception $e) {
        watchdog_exception('hybridauth', $e);
      }
    }

    return MENU_ACCESS_DENIED;
  }

  public function hybridauth_window_start($provider_id) {
    // If provider is OpenID, but we don't have the openid_identifier.
    if ($provider_id == 'OpenID' && !isset($_GET['openid_identifier'])) {
      $form = drupal_get_form('hybridauth_openid_form');
      return _hybridauth_window_render_form($form, $provider_id);
    }

    // Make sure the session is started, HybridAuth library needs it.
    // hybridauth_session_start();
    session_start();
    // Try to get HybridAuth instance.
    if ($hybridauth = hybridauth_get_instance($provider_id)) {
      return $this->_hybridauth_window_auth($hybridauth, $provider_id);
    }
    else {
      drupal_set_message(t('There was an error processing your request.'), 'error');
      _hybridauth_window_close(FALSE);
    }
  }

  public function hybridauth_providers($js, $icon_pack) {
    $build = [
      '#type' => 'hybridauth_widget',
      '#title' => '',
      '#hybridauth_widget_type' => 'list',
      '#hybridauth_widget_icon_pack' => $icon_pack,
    ];

    if ($js) {
      ctools_include('modal');
      ctools_modal_render(t('Log in using your account with'), $build);
    }
    else {
      $build['#title'] = t('Log in using your account with');
      return $build;
    }
  }

  public function hybridauth_user_identity($account) {
    global $base_url;
    $header = [t('Authenticatdddion provider'), t('Identity'), t('Delete')];
    $rows = [];

    $identities = _hybridauth_identity_load_by_uid($account);
    foreach ($identities as $key => $identity) {
      $data = unserialize($identity['data']);
      $url = \Drupal\Core\Url::fromUri($data['profileURL']);
      $external_link = \Drupal::l(t($data['profileURL']), $url);
      $delete = $base_url . '/user/' . $account . '/hybridauth/delete/' . $identity['id'];
      $deleteurl = \Drupal\Core\Url::fromUri($delete);
      $delete_link = \Drupal::l(t('Delete'), $deleteurl);
      $rows[$data['provider']] = [
        $data['provider'],
        $external_link,
        $delete_link,
      ];
    }

    $output['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No identities'),
    ];
    return [
      '#type' => 'markup',
      '#markup' => drupal_render($output),
    ];

    // Add more identities.
    if ($account->id() == $user->uid && user_access('use hybridauth')) {
      $build['hybridauth_widget'] = [
        '#type' => 'hybridauth_widget',
        '#title' => t('Add more identities'),
        '#weight' => 10,
        '#hybridauth_widget_type' => 'list',
        '#hybridauth_destination' => '',
        '#hybridauth_destination_error' => '',
      ];
    }

    if (variable_get('hybridauth_debug', 0)) {
      $connected_providers = hybridauth_get_connected_providers();
      $build['connected_providers'] = [
        '#markup' => t('Currently connected to (session data):') . ' ' . implode(', ', $connected_providers),
        '#weight' => 15,
      ];
    }

    // Tokens browser for admins.
    if (user_access('administer site configuration') || user_access('administer users')) {
      $build['vtabs'] = [
        '#type' => 'vertical_tabs',
        '#weight' => 20,
      ];

      $header = [t('Token'), t('Value')];
      // User tokens.
      if (!empty($account->data['hybridauth'])) {
        $build['vtabs']['fset_user_tokens'] = [
          '#type' => 'fieldset',
          '#title' => t('User tokens'),
        ];

        $rows = [];
        foreach ($account->data['hybridauth'] as $key => $value) {
          $rows[] = ['[user:hybridauth:' . $key . ']', $value];
        }
        $build['vtabs']['fset_user_tokens']['tokens'] = [
          '#theme' => 'table',
          '#header' => $header,
          '#rows' => $rows,
          '#sticky' => FALSE,
        ];
      }

      // Data from auth providers.
      foreach ($data_array as $data) {
        $build['vtabs']['fset_' . $data['provider'] . '_' . $data['identifier']] = [
          '#type' => 'fieldset',
          '#title' => $providers[$data['provider']],
        ];

        $rows = [];
        foreach ($data as $key => $value) {
          $rows[] = [$key, $value];
        }
        $build['vtabs']['fset_' . $data['provider'] . '_' . $data['identifier']]['tokens'] = [
          '#theme' => 'table',
          '#header' => $header,
          '#rows' => $rows,
          '#sticky' => FALSE,
        ];
      }
    }

    return $build;
  }

  function _hybridauth_window_auth(\Hybrid_Auth $hybridauth, $provider_id) {
    $error_code = NULL;
    if (is_object($hybridauth)) {
      // @FIXME
// url() expects a route name or an external URI.
// $params = array(
//       'hauth_return_to' => url('hybridauth/window/' . $provider_id, array('absolute' => TRUE, 'query' => \Drupal\Component\Utility\UrlHelper::filterQueryParameters())),
//     );

      if (isset($_GET['openid_identifier'])) {
        $params['openid_identifier'] = $_GET['openid_identifier'];
      }
      try {
        $adapter = $hybridauth->authenticate($provider_id, $params);
        $aa = '';
        $profile = (array) ($adapter->getUserProfile());
        $a = '';
      }
      catch (\Exception $e) {
        watchdog_exception('hybridauth', $e);
        $error_code = $e->getCode();
      }
    }
    else {
      $error_code = $hybridauth;
    }

    if (!is_null($error_code)) {
      // Destroy the session started in hybridauth_window_start() if user is not
      // logged in.
      if (!\Drupal::currentUser()->isAuthenticated()) {
        // Delete session only if it contains just HybridAuth data.
        $delete_session = TRUE;
        foreach ($_SESSION as $key => $value) {
          if (substr($key, 0, 4) != 'HA::') {
            $delete_session = FALSE;
          }
        }
        if ($delete_session) {
          session_destroy();
        }
      }
      switch ($error_code) {
        case 5:
          // Authentication failed. The user has canceled the authentication or
          // the provider refused the connection.
          break;

        case 0:
        // Unspecified error.
        case 1:
        // Hybridauth configuration error.
        case 2:
        // Provider not properly configured.
        case 3:
        // Unknown or disabled provider.
        case 4:
        // Missing provider application credentials (your application id, key
        // or secret).
        case 6:
        // User profile request failed.
        case 7:
        // User not connected to the provider.
        case 8:
        // Provider does not support this feature.
        default:
          // Report the error - this message is not shown to anonymous users as
          // we destroy the session - see below.
          drupal_set_message(t('There was an error processing your request.'), 'error');
      }

      $this->_hybridauth_window_close(FALSE);
    }

    $profile['provider'] = $provider_id;
    // Invoke hook_hybridauth_profile_alter().
    \Drupal::moduleHandler()->alter('hybridauth_profile', $profile);
    // Process Drupal authentication.
    return $this->_hybridauth_window_process_auth($profile);
    //return 1;
  }

  /**
   * Handle the Drupal authentication.
   */
  function _hybridauth_window_process_auth($data) {
    global $base_url;
    if ($identity = $this->_hybridauth_identity_load($data)) {
      $user = \Drupal::entityManager()->getStorage('user')->load($identity['uid']);
      user_login_finalize($user);
    }
    else {
      $user = User::create();
      $user->setPassword('test');
      $user->enforceIsNew();
      $user->setEmail('test@test.com');
      $user->setUsername($data['firstName']);
      $user->block();
      $user->save();
      $this->_hybridauth_identity_save($data, $user->id());
      user_login_finalize($user);
    }
    $url = $base_url.'/user/' . $user->id() . '/hybridauth';
    return (new RedirectResponse($url))->send();
    /*    // User is already logged in, tries to add new identity.
      if (\Drupal::currentUser()->isAuthenticated()) {
      // Identity is already registered.
      if ($identity = _hybridauth_identity_load($data)) {
      // Registered to this user.
      if ($user->uid == $identity['uid']) {
      drupal_set_message(t('You have already registered this identity.'));
      _hybridauth_window_close();
      }
      // Registered to another user.
      else {
      drupal_set_message(t('This identity is registered to another user.'), 'error');
      _hybridauth_window_close();
      }
      }
      // Identity is not registered - add it to the logged in user.
      else {
      _hybridauth_identity_save($data);
      drupal_set_message(t('New identity added.'));
      _hybridauth_invoke_hooks('hybridauth_identity_added', $user, $data);
      _hybridauth_window_close();
      }
      }

      if ($identity = _hybridauth_identity_load($data)) {
      // Check if user is blocked.
      if ($account = _hybridauth_user_is_blocked_by_uid($identity['uid'])) {
      drupal_set_message(t('The username %name has not been activated or is blocked.', array('%name' => $account->name)), 'error');
      }
      // Check for email verification timestamp.
      elseif (!_hybridauth_user_login_access_by_uid($identity['uid'])) {
      $data = unserialize($identity['data']);
      drupal_set_message(t('You need to verify your e-mail address - !email.', array('!email' => $data['email'])), 'error');
      drupal_set_message(t('A welcome message with further instructions has been sent to your e-mail address.'));
      _hybridauth_mail_notify('hybridauth_email_verification', \Drupal::entityManager()->getStorage('user')->load($identity['uid']));
      }
      else {
      $form_state['uid'] = $identity['uid'];
      user_login_submit(array(), $form_state);
      _hybridauth_invoke_hooks('hybridauth_user_login', $user, $data);
      }
      }
      // Handle duplicate email addresses.
      elseif (\Drupal::config('hybridauth.settings')->get('hybridauth_duplicate_emails') && !empty($data['email']) && $account = user_load_by_mail($data['email'])) {
      // Add identity to existing account, only if emailVerified.
      if (\Drupal::config('hybridauth.settings')->get('hybridauth_duplicate_emails') == 2 && $data['email'] == $data['emailVerified']) {
      _hybridauth_identity_save($data, $account->uid);
      drupal_set_message(t('New identity added.'));
      _hybridauth_invoke_hooks('hybridauth_identity_added', $account, $data);
      $form_state['uid'] = $account->uid;
      user_login_submit(array(), $form_state);
      _hybridauth_invoke_hooks('hybridauth_user_login', $user, $data);
      }
      // Block registration - if (variable_get('hybridauth_duplicate_emails', 1) == 1) or
      // (variable_get('hybridauth_duplicate_emails', 1) == 2 && $data['email'] != $data['emailVerified'])
      else {
      drupal_set_message(t('You are trying to login with email address of another user.'), 'error');
      if (!empty($account->data['hybridauth'])) {
      $providers = hybridauth_providers_list();
      drupal_set_message(t('If you are completely sure it is your email address, try to login through %provider.',
      array('%provider' => $providers[$account->data['hybridauth']['provider']])), 'status');
      }
      else {
      drupal_set_message(t('If you are completely sure it is your email address, try to login using your username and password on this site. If you don\'t remember your password - <a href="@password">request new password</a>.',
      array('@password' => \Drupal\Core\Url::fromRoute('user.pass'))));
      }
      }
      }
      // Check if other modules want to block this registration.
      elseif ($message = _hybridauth_registration_block($data)) {
      // Destroy the session with the blocked authorized identity.
      session_destroy();
      if (is_string($message)) {
      drupal_set_message($message, 'error');
      }
      }
      // Create new user account.
      else {
      // Visitors can create accounts.
      // @FIXME
      // // @FIXME
      // // This looks like another module's variable. You'll need to rewrite this call
      // // to ensure that it uses the correct configuration object.
      // if ((\Drupal::config('hybridauth.settings')->get('hybridauth_register') == 0 && variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL))
      //       || variable_get('hybridauth_register', 0) == 1 || variable_get('hybridauth_register', 0) == 2) {
      //       _hybridauth_invoke_hooks('hybridauth_user_preinsert', $user, $data);
      //
      //       // Check profile information for required fields.
      //       if ($additional_info = _hybridauth_check_additional_info($data)) {
      //         return $additional_info;
      //       }
      //
      //       // As we have already checked for the additional info we can unset the plaintext $data['pass'] here.
      //       if (isset($data['pass'])) {
      //         $user_password = $data['pass'];
      //         unset($data['pass']);
      //       }
      //
      //       // TODO: remove this global if possible.
      //       global $_hybridauth_data;
      //       $_hybridauth_data = $data;
      //       // Register this new user.
      //       $name = _hybridauth_make_username($data);
      //       $userinfo = array(
      //         'name' => $name,
      //         'pass' => empty($user_password) ? user_password() : $user_password,
      //         'init' => $data['email'],
      //         'status' => 1,
      //         'access' => REQUEST_TIME,
      //         'mail' => $data['email'],
      //         'data' => array('hybridauth' => $data),
      //       );
      //       // Invoke hook_hybridauth_userinfo_alter().
      //       \Drupal::moduleHandler()->alter('hybridauth_userinfo', $userinfo, $data);
      //
      //       $admin_approval_required = FALSE;
      //       // Admin approval is required.
      //       if ((variable_get('hybridauth_register', 0) == 0 && variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL) == USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL)
      //         || variable_get('hybridauth_register', 0) == 2) {
      //         $userinfo['status'] = 0;
      //         $admin_approval_required = TRUE;
      //       }
      //       $account = user_save(drupal_anonymous_user(), $userinfo);
      //       // Terminate if an error occurred during user_save().
      //       if (!$account) {
      //         drupal_set_message(t('Error saving user account.'), 'error');
      //         _hybridauth_window_close();
      //       }
      //       _hybridauth_invoke_hooks('hybridauth_user_insert', $account, $data);
      //       _hybridauth_identity_save($data, $account->uid);
      //       _hybridauth_invoke_hooks('hybridauth_identity_added', $account, $data);
      //
      //       $user_save_trigger = FALSE;
      //       $user_email_verify_trigger = FALSE;
      //       $user_login_trigger = TRUE;
      //       // Save user picture.
      //       if (variable_get('user_pictures', 0) && variable_get('hybridauth_pictures', 1)) {
      //         $photo_url = $data['photoURL'];
      //         if (valid_url($photo_url)) {
      //           $photo = drupal_http_request($photo_url);
      //           if (isset($photo->error)) {
      //             watchdog('hybridauth', 'Error while executing drupal_http_request() to %url: %error.', array('%url' => $photo_url, '%error' => $photo->error), WATCHDOG_ERROR);
      //           }
      //           else {
      //             if ($file = file_save_data($photo->data)) {
      //               // To make user_save() to process the file and move it.
      //               $file->status = 0;
      //               $edit['picture'] = $file;
      //               $user_save_trigger = TRUE;
      //             }
      //             else {
      //               watchdog('hybridauth', 'Failed to save user image from url %url.', array('%url' => $photo_url), WATCHDOG_ERROR);
      //             }
      //           }
      //         }
      //       }
      //       // Admin approval is required.
      //       if ($admin_approval_required) {
      //         $user_login_trigger = FALSE;
      //         _user_mail_notify('register_pending_approval', $account);
      //         drupal_set_message(t('Thank you for applying for an account. Your account is currently pending approval by the site administrator.<br />In the meantime, a welcome message with further instructions has been sent to your e-mail address.'));
      //       }
      //       // Email verification is required.
      //       elseif (!empty($data['email']) && $data['email'] != $data['emailVerified']
      //         && ((!variable_get('hybridauth_email_verification', 0) && variable_get('user_email_verification', TRUE)) || variable_get('hybridauth_email_verification', 0) == 1)) {
      //         $user_login_trigger = FALSE;
      //         // Dries birthday timestamp, Nov 19, 1978 = 280281600 :).
      //         $edit['login'] = 280281600;
      //         $user_save_trigger = TRUE;
      //         $user_email_verify_trigger = TRUE;
      //       }
      //
      //       if ($user_save_trigger) {
      //         // Hack to remove one notice from Legal module.
      //         if (\Drupal::moduleHandler()->moduleExists('legal')) {
      //           $edit['legal_accept'] = NULL;
      //         }
      //         $account = user_save($account, $edit);
      //       }
      //       if ($user_email_verify_trigger) {
      //         _hybridauth_mail_notify('hybridauth_email_verification', $account);
      //         drupal_set_message(t('A welcome message with further instructions has been sent to your e-mail address.'));
      //       }
      //
      //       // Log user in.
      //       if ($user_login_trigger) {
      //         $form_state['uid'] = $account->uid;
      //         user_login_submit(array(), $form_state);
      //         _hybridauth_invoke_hooks('hybridauth_user_login', $user, $data);
      //       }
      //     }
      //     // Visitors can't register accounts through HybridAuth.
      //     elseif (variable_get('hybridauth_register', 0) == 3) {
      //       if (variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL)) {
      //         $message = t('Sorry, you are not allowed to login. Please, <a href="@register">create a new account</a>.', array('@register' => url('user/register')));
      //       }
      //       else {
      //         $message = t('New account registration is not allowed.');
      //       }
      //       drupal_set_message($message, 'error');
      //       _hybridauth_window_close(FALSE);
      //     }
      //     // Only admin can create accounts.
      //     else {
      //       drupal_set_message(t('Only site administrators can create new user accounts.'), 'error');
      //       _hybridauth_window_close(FALSE);
      //     }

      } */
  }

  /**
   * Close the popup (if used) and redirect if there was no error.
   *//*
    function _hybridauth_window_close($redirect = TRUE) {
    $user = \Drupal::currentUser();
    // Prevent devel module from spewing.
    $GLOBALS['devel_shutdown'] = FALSE;

    $destination = drupal_get_destination();
    $destination = $destination['destination'];
    // Check if token replacements are allowed for the destination string.
    if (_hybridauth_allow_token_replace($destination)) {
    $destination = \Drupal::token()->replace($destination, array('user' => $user), array('clear' => TRUE));
    }
    $destination_error = !empty($_GET['destination_error']) ? $_GET['destination_error'] : \Drupal::config('hybridauth.settings')->get('hybridauth_destination_error');

    $base_options = array(
    'absolute' => TRUE,
    // The redirect target must never be an external URL to prevent open
    // redirect vulnerabilities.
    'external' => FALSE,
    );
    $destination_options = \Drupal\Component\Utility\UrlHelper::parse($destination) + $base_options;
    $destination_error_options = \Drupal\Component\Utility\UrlHelper::parse($destination_error) + $base_options;

    // @FIXME
    // url() expects a route name or an external URI.
    // $destination = url($destination_options['path'], $destination_options);

    // @FIXME
    // url() expects a route name or an external URI.
    // $destination_error = url($destination_error_options['path'], $destination_error_options);


    // @FIXME
    // The Assets API has totally changed. CSS, JavaScript, and libraries are now
    // attached directly to render arrays using the #attached property.
    //
    //
    // @see https://www.drupal.org/node/2169605
    // @see https://www.drupal.org/node/2408597
    // drupal_add_js(array(
    //     'hybridauth'=> array(
    //       'redirect' => $redirect ? 1 : 0,
    //       'destination' => $destination,
    //       'destination_error' => $destination_error,
    //     )
    //   ), 'setting');

    // @FIXME
    // The Assets API has totally changed. CSS, JavaScript, and libraries are now
    // attached directly to render arrays using the #attached property.
    //
    //
    // @see https://www.drupal.org/node/2169605
    // @see https://www.drupal.org/node/2408597
    // drupal_add_js(drupal_get_path('module', 'hybridauth') . '/js/hybridauth.close.js');


    // Make sure that we send the correct content type with charset, otherwise
    // Firefox might repeat the GET request.
    // @see https://www.drupal.org/node/2648912
    $a = '';
    //drupal_add_http_header('Content-Type', 'text/html; charset=utf-8');
    \Drupal::request()->headers->set('Content-Type', 'text/html; charset=utf-8');

    $page = element_info('page');
    // Don't show messages on this closing page, show them later.
    $page['#show_messages'] = FALSE;
    $page['#children'] = t('Closing...');
    // @FIXME
    // theme() has been renamed to _theme() and should NEVER be called directly.
    // Calling _theme() directly can alter the expected output and potentially
    // introduce security issues (see https://www.drupal.org/node/2195739). You
    // should use renderable arrays instead.
    //
    //
    // @see https://www.drupal.org/node/2195739
    // print theme('html', array('page' => $page));

    //drupal_exit();
    throw new ServiceUnavailableHttpException();
    }
   */
  function _hybridauth_identity_save($data, $uid = NULL) {
    $user = \Drupal::currentUser();
    $uid = $uid ? $uid : $user->id();
    db_merge('hybridauth_identity')
        ->key(array('uid' => $uid, 'provider' => $data['provider'], 'provider_identifier' => _hybridauth_provider_identifier($data['identifier'])))
        ->fields(array('data' => serialize($data)))
        ->execute();
  }

  function _hybridauth_identity_load($data) {
    $result = db_select('hybridauth_identity', 'ha_id')
        ->fields('ha_id')
        ->condition('provider', $data['provider'], '=')
        ->condition('provider_identifier', _hybridauth_provider_identifier($data['identifier']), '=')
        ->execute()
        ->fetchAssoc();
    return $result;
  }

}
