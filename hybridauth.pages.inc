<?php
/**
 * @file
 * HybridAuth module pages.
 */

function hybridauth_endpoint() {
  // Make sure the session is started, HybridAuth library needs it.
  hybridauth_session_start();

  if ($lib_path = _hybridauth_library_path()) {
    try {
      // If Composer install was executed in the Hybridauth library use that
      // autoloader.
      if (file_exists($lib_path . '/../vendor/autoload.php')) {
        require_once $lib_path . '/../vendor/autoload.php';
      }
      require_once $lib_path . '/index.php';
    }
    catch(Exception $e) {
      watchdog_exception('hybridauth', $e);
    }
  }

  return MENU_ACCESS_DENIED;
}

/**
 * Initialize the user session before using the HybridAuth library.
 */
function hybridauth_session_start() {
  // Make sure that a user session exists.
  drupal_session_start();
  // Special handling for HTTPS with normal session IDs and secure session IDs.
  // Duplicated sessions are created, so we need to pull out the correct session
  // data.
  global $is_https;
  // @FIXME
// // @FIXME
// // This looks like another module's variable. You'll need to rewrite this call
// // to ensure that it uses the correct configuration object.
// if ($is_https && variable_get('https', FALSE)) {
//     // If there is an existing session with the same secure session ID we need
//     // to replace the $_SESSION contents with that.
//     $session = db_query('SELECT session FROM {sessions} WHERE sid = :sid AND ssid = :sid', array('sid' => session_id()))->fetchField();
//     if ($session) {
//       // Overwrite $_SESSION with the data.
//       session_decode($session);
//       // Remove the duplicate session from the database.
//       db_delete('sessions')->condition('sid', session_id())->execute();
//     }
//   }

}

/**
 * Returns HybridAuth widget with list of providers icons.
 */
function hybridauth_providers($js, $icon_pack) {
  $build = array(
    '#type' => 'hybridauth_widget',
    '#title' => '',
    '#hybridauth_widget_type' => 'list',
    '#hybridauth_widget_icon_pack' => $icon_pack,
  );

  if ($js) {
    // @FIXME
// Most CTools APIs have been moved into core.
// 
// @see https://www.drupal.org/node/2164623
// ctools_include('modal');

    ctools_modal_render(t('Log in using your account with'), $build);
  }
  else {
    $build['#title'] = t('Log in using your account with');
    return $build;
  }
}

function hybridauth_window_start($provider_id) {
  // If provider is OpenID, but we don't have the openid_identifier.
  if ($provider_id == 'OpenID' && !isset($_GET['openid_identifier'])) {
    $form = \Drupal::formBuilder()->getForm('hybridauth_openid_form');
    return _hybridauth_window_render_form($form, $provider_id);
  }

  // Make sure the session is started, HybridAuth library needs it.
  hybridauth_session_start();

  // Try to get HybridAuth instance.
  if ($hybridauth = hybridauth_get_instance()) {
    return _hybridauth_window_auth($hybridauth, $provider_id);
  }
  else {
    drupal_set_message(t('There was an error processing your request.'), 'error');
    _hybridauth_window_close(FALSE);
  }
}

/**
 * Close the popup (if used) and redirect if there was no error.
 */
function _hybridauth_window_close($redirect = TRUE) {
  $user = \Drupal::currentUser();
  // Prevent devel module from spewing.
  $GLOBALS['devel_shutdown'] = FALSE;

  $destination = drupal_get_destination();
  $destination = $destination['destination'];
  // Check if token replacements are allowed for the destination string.
  if (_hybridauth_allow_token_replace($destination)) {
    $destination = \Drupal::token()->replace($destination, array('user' => $user), array('clear' => TRUE));
  }
  $destination_error = !empty($_GET['destination_error']) ? $_GET['destination_error'] : \Drupal::config('hybridauth.settings')->get('hybridauth_destination_error');

  $base_options = array(
    'absolute' => TRUE,
    // The redirect target must never be an external URL to prevent open
    // redirect vulnerabilities.
    'external' => FALSE,
  );
  $destination_options = \Drupal\Component\Utility\UrlHelper::parse($destination) + $base_options;
  $destination_error_options = \Drupal\Component\Utility\UrlHelper::parse($destination_error) + $base_options;

  // @FIXME
// url() expects a route name or an external URI.
// $destination = url($destination_options['path'], $destination_options);

  // @FIXME
// url() expects a route name or an external URI.
// $destination_error = url($destination_error_options['path'], $destination_error_options);


  // @FIXME
// The Assets API has totally changed. CSS, JavaScript, and libraries are now
// attached directly to render arrays using the #attached property.
// 
// 
// @see https://www.drupal.org/node/2169605
// @see https://www.drupal.org/node/2408597
// drupal_add_js(array(
//     'hybridauth'=> array(
//       'redirect' => $redirect ? 1 : 0,
//       'destination' => $destination,
//       'destination_error' => $destination_error,
//     )
//   ), 'setting');

  // @FIXME
// The Assets API has totally changed. CSS, JavaScript, and libraries are now
// attached directly to render arrays using the #attached property.
// 
// 
// @see https://www.drupal.org/node/2169605
// @see https://www.drupal.org/node/2408597
// drupal_add_js(drupal_get_path('module', 'hybridauth') . '/js/hybridauth.close.js');


  // Make sure that we send the correct content type with charset, otherwise
  // Firefox might repeat the GET request.
  // @see https://www.drupal.org/node/2648912
  drupal_add_http_header('Content-Type', 'text/html; charset=utf-8');

  $page = element_info('page');
  // Don't show messages on this closing page, show them later.
  $page['#show_messages'] = FALSE;
  $page['#children'] = t('Closing...');
  // @FIXME
// theme() has been renamed to _theme() and should NEVER be called directly.
// Calling _theme() directly can alter the expected output and potentially
// introduce security issues (see https://www.drupal.org/node/2195739). You
// should use renderable arrays instead.
// 
// 
// @see https://www.drupal.org/node/2195739
// print theme('html', array('page' => $page));

  drupal_exit();
}

function _hybridauth_check_additional_info($data) {
  $show_form = FALSE;

    if (empty($data['username']) && \Drupal::config('hybridauth.settings')->get('hybridauth_registration_username_change')) {
    $show_form = TRUE;
  }
  if (empty($data['pass']) && \Drupal::config('hybridauth.settings')->get('hybridauth_registration_password')) {
    $show_form = TRUE;
  }
  // @FIXME
// Could not extract the default value because it is either indeterminate, or
// not scalar. You'll need to provide a default value in
// config/install/hybridauth.settings.yml and config/schema/hybridauth.schema.yml.
$required_fields = array_filter(\Drupal::config('hybridauth.settings')->get('hybridauth_required_fields'));
  foreach ($required_fields as $key => $value) {
    if (empty($data[$key]) && !($data[$key] === 0)) {
      $show_form = TRUE;
      break;
    }
  }
  // Allow other modules to show pre-registration form.
  // Invoke hook_hybridauth_registration_form().
  foreach (\Drupal::moduleHandler()->invokeAll('hybridauth_registration_form', [$data]) as $value) {
    if ($value) {
      $show_form = TRUE;
    }
  }

  if ($show_form) {
    $form = \Drupal::formBuilder()->getForm('hybridauth_additional_info_form', $data);
    return _hybridauth_window_render_form($form, $data['provider']);
  }
}

function _hybridauth_window_render_form($form, $provider_id) {
  // @FIXME
// // @FIXME
// // The correct configuration object could not be determined. You'll need to
// // rewrite this call manually.
// $window_type = variable_get('hybridauth_provider_' . $provider_id . '_window_type', 'current');


  if ($window_type == 'current') {
    return $form;
  }
  else { // 'popup' or modal ('colorbox', 'shadowbox', 'fancybox', 'lightbox2')
    $page = element_info('page');
    // @FIXME
// theme() has been renamed to _theme() and should NEVER be called directly.
// Calling _theme() directly can alter the expected output and potentially
// introduce security issues (see https://www.drupal.org/node/2195739). You
// should use renderable arrays instead.
// 
// 
// @see https://www.drupal.org/node/2195739
// $page['#children'] = theme('status_messages') . \Drupal::service("renderer")->render($form);

    // @FIXME
// theme() has been renamed to _theme() and should NEVER be called directly.
// Calling _theme() directly can alter the expected output and potentially
// introduce security issues (see https://www.drupal.org/node/2195739). You
// should use renderable arrays instead.
// 
// 
// @see https://www.drupal.org/node/2195739
// print theme('html', array('page' => $page));

  }

  drupal_exit();
}

function hybridauth_additional_info_form($form, &$form_state, $data) {
  $form['data'] = array(
    '#type' => 'value',
    '#value' => $data,
  );
  $form['fset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Required information'),
    '#description' => t('Please fill in additional information to complete your registration.'),
  );

  if (\Drupal::config('hybridauth.settings')->get('hybridauth_registration_username_change')) {
    $form['fset']['username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#maxlength' => USERNAME_MAX_LENGTH,
      '#required' => TRUE,
      '#attributes' => array('class' => array('username')),
      '#default_value' => _hybridauth_make_username($data, TRUE),
      '#description' => t('Choose your username.') . ' '
        . t('Spaces are allowed; punctuation is not allowed except for periods, hyphens, apostrophes, and underscores.'),
    );
    if (\Drupal::moduleHandler()->moduleExists('username_check')) {
      _username_check_load_resources('auto');
      $form['fset']['username']['#field_suffix'] = '<span id="username-check-informer">&nbsp;</span>';
      $form['fset']['username']['#suffix'] = '<div id="username-check-message"></div>';
    }
    elseif (\Drupal::moduleHandler()->moduleExists('friendly_register')) {
      friendly_register_load_resources();
      $form['fset']['username']['#attributes']['class'][] = 'friendly-register-name';
    }
  }
  if (\Drupal::config('hybridauth.settings')->get('hybridauth_registration_password')) {
    $form['fset']['pass'] = array(
      '#type' => 'password_confirm',
      //'#title' => t('Password'),
      '#required' => TRUE,
    );
  }

  $hybridauth_fields = hybridauth_fields_list();
  // @FIXME
// Could not extract the default value because it is either indeterminate, or
// not scalar. You'll need to provide a default value in
// config/install/hybridauth.settings.yml and config/schema/hybridauth.schema.yml.
$required_fields = array_filter(\Drupal::config('hybridauth.settings')->get('hybridauth_required_fields'));
  foreach ($required_fields as $key => $value) {
    if (empty($data[$key]) && !($data[$key] === 0)) {
      $form['fset'][$key] = array(
        '#type' => 'textfield',
        '#title' => $hybridauth_fields[$key],
        '#required' => TRUE,
      );
      if ($key == 'email') {
        $form['fset'][$key]['#maxlength'] = EMAIL_MAX_LENGTH;
        $form['fset'][$key]['#description'] = t('A valid e-mail address. All e-mails from the system will be sent to this address. The e-mail address is not made public and will only be used if you wish to receive a new password or wish to receive certain news or notifications by e-mail.');
      }
      if ($key == 'gender') {
        $form['fset'][$key]['#type'] = 'radios';
        $form['fset'][$key]['#options'] = array(
          'male' => t('Male'),
          'female' => t('Female'),
        );
      }
    }
  }

  $form['fset']['actions'] = array('#type' => 'actions');
  $form['fset']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function hybridauth_additional_info_form_validate($form, &$form_state) {
  // Validate username.
  if (isset($form_state['values']['username'])) {
    if ($error = user_validate_name($form_state['values']['username'])) {
      form_set_error('username', $error);
    }
    elseif (user_load_by_name($form_state['values']['username'])) {
      form_set_error('username', t('The name %name is already taken.', array('%name' => $form_state['values']['username'])));
    }
  }

  if (isset($form_state['values']['email'])) {
    // Trim whitespace from mail, to prevent confusing 'e-mail not valid'
    // warnings often caused by cutting and pasting.
    $mail = trim($form_state['values']['email']);
    $form_state->setValueForElement($form['fset']['email'], $mail);

    // Validate the e-mail address.
    if ($error = user_validate_mail($form_state['values']['email'])) {
      form_set_error('email', $error);
    }
  }
}

function hybridauth_additional_info_form_submit($form, &$form_state) {
  $data = $form_state['values']['data'];
  // @FIXME
// Could not extract the default value because it is either indeterminate, or
// not scalar. You'll need to provide a default value in
// config/install/hybridauth.settings.yml and config/schema/hybridauth.schema.yml.
$required_fields = array_filter(\Drupal::config('hybridauth.settings')->get('hybridauth_required_fields'));

  foreach ($required_fields as $key => $value) {
    if (empty($data[$key]) && !($data[$key] === 0)) {
      $data[$key] = $form_state['values'][$key];
      if (!isset($data['manual']) || is_array($data['manual'])) {
        $data['manual'][] = $key;
      }
    }
  }

  if (isset($form_state['values']['username'])) {
    $data['username'] = $form_state['values']['username'];
    if (!isset($data['manual']) || is_array($data['manual'])) {
      $data['manual'][] = 'username';
    }
  }
  if (isset($form_state['values']['pass'])) {
    $data['pass'] = $form_state['values']['pass'];
    if (!isset($data['manual']) || is_array($data['manual'])) {
      $data['manual'][] = 'pass';
    }
  }

  if (isset($data['manual']) && is_array($data['manual'])) {
    $data['manual'] = implode(',', $data['manual']);
  }

  _hybridauth_window_process_auth($data);
}

function hybridauth_openid_form($form, &$form_state) {
  $form['openid_identifier'] = array(
    '#type' => 'textfield',
    '#title' => t('OpenID Identity'),
    '#description' => t('Type your OpenID identity you want to use.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function hybridauth_openid_form_submit($form, &$form_state) {
  $query = \Drupal\Component\Utility\UrlHelper::filterQueryParameters() + drupal_get_destination();
  // Unset destination in global $_GET so that drupal_goto() doesn't use it.
  unset($_GET['destination']);
  $query['openid_identifier'] = $form_state['values']['openid_identifier'];

  drupal_goto('hybridauth/window/OpenID', array('query' => $query));
}

/**
 * Menu callback; manage HybridAuth identities for the specified user.
 */
function hybridauth_user_identity(\Drupal\user\UserInterface $account) {
  $user = \Drupal::currentUser();
  // @FIXME
// drupal_set_title() has been removed. There are now a few ways to set the title
// dynamically, depending on the situation.
// 
// 
// @see https://www.drupal.org/node/2067859
// drupal_set_title(format_username($account));


  $identities = _hybridauth_identity_load_by_uid($account->id());
  $providers = hybridauth_providers_list();

  $header = array(t('Authentication provider'), t('Identity'), t('Delete'));
  $rows = array();
  $data_array = array();
  foreach ($identities as $identity) {
    $data = unserialize($identity['data']);
    $data_array[] = $data;
    // @FIXME
// l() expects a Url object, created from a route name or external URI.
// $rows[] = array(
//       $providers[$data['provider']],
//       l($data['profileURL'], $data['profileURL'], array('attributes' => array('target' => '_blank'), 'external' => TRUE)),
//       l(t('Delete'), 'user/' . $account->id() . '/hybridauth/delete/' . $identity['id']),
//     );

  }

  $build = array();
  $build['identity'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#sticky' => FALSE,
    '#empty' => t("You don't have any identities yet."),
  );

  // Add more identities.
  if ($account->id() == $user->uid && \Drupal::currentUser()->hasPermission('use hybridauth')) {
    $build['hybridauth_widget'] = array(
      '#type' => 'hybridauth_widget',
      '#title' => t('Add more identities'),
      '#weight' => 10,
      '#hybridauth_widget_type' => 'list',
      '#hybridauth_destination' => '',
      '#hybridauth_destination_error' => '',
    );
  }

  if (\Drupal::config('hybridauth.settings')->get('hybridauth_debug')) {
    $connected_providers = hybridauth_get_connected_providers();
    $build['connected_providers'] = array(
      '#markup' => t('Currently connected to (session data):') . ' ' . implode(', ', $connected_providers),
      '#weight' => 15,
    );
  }

  // Tokens browser for admins.
  if (\Drupal::currentUser()->hasPermission('administer site configuration') || \Drupal::currentUser()->hasPermission('administer users')) {
    $build['vtabs'] = array(
      '#type' => 'vertical_tabs',
      '#weight' => 20,
    );

    $header = array(t('Token'), t('Value'));
    // User tokens.
    if (!empty($account->data['hybridauth'])) {
      $build['vtabs']['fset_user_tokens'] = array(
        '#type' => 'fieldset',
        '#title' => t('User tokens'),
      );

      $rows = array();
      foreach ($account->data['hybridauth'] as $key => $value) {
        $rows[] = array('[user:hybridauth:' . $key . ']', $value);
      }
      $build['vtabs']['fset_user_tokens']['tokens'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#sticky' => FALSE,
      );
    }

    // Data from auth providers.
    foreach ($data_array as $data) {
      $build['vtabs']['fset_' . $data['provider'] . '_' . $data['identifier']] = array(
        '#type' => 'fieldset',
        '#title' => $providers[$data['provider']],
      );

      $rows = array();
      foreach ($data as $key => $value) {
        $rows[] = array($key, $value);
      }
      $build['vtabs']['fset_' . $data['provider'] . '_' . $data['identifier']]['tokens'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#sticky' => FALSE,
      );
    }
  }

  return $build;
}

function hybridauth_user_identity_delete($form, &$form_state, $account, $id) {
  $del_identity = _hybridauth_identity_load_by_id($id);
  if (!$del_identity || $account->uid != $del_identity['uid']) {
    drupal_set_message(t('You are trying to delete non-existing identity.'), 'error');
    drupal_access_denied();
    // See https://drupal.org/node/2020351 and
    // https://api.drupal.org/api/drupal/includes%21common.inc/function/drupal_access_denied/7
    drupal_exit();
  }
  $del_identity_data = unserialize($del_identity['data']);
  $username = format_username($account);
  // @FIXME
// l() expects a Url object, created from a route name or external URI.
// $question = t('Are you sure you want to detach the HybridAuth identity !identity from %user?',
//     array(
//       '!identity' => l($del_identity_data['profileURL'], $del_identity_data['profileURL'], array('attributes' => array('target' => '_blank'), 'external' => TRUE)),
//       '%user' => $username));


  $form = array();
  $form['#user'] = $account;
  $form['#del_identity'] = $del_identity;
  $form['#del_identity_data'] = $del_identity_data;
  $form['question'] = array(
    '#markup' => $question,
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );

  if (!empty($account->data['hybridauth']) && $account->data['hybridauth']['provider'] == $del_identity_data['provider'] &&
    $account->data['hybridauth']['identifier'] == $del_identity_data['identifier']) {
    $identities = _hybridauth_identity_load_by_uid($account->uid);
    $providers = hybridauth_providers_list();
    $options = array();
    foreach ($identities as $key => $identity) {
      $data = unserialize($identity['data']);
      if ($key != $id) {
        // @FIXME
// l() expects a Url object, created from a route name or external URI.
// $options[$key] = $providers[$identity['provider']] . ' - ' . l($data['profileURL'], $data['profileURL'], array('attributes' => array('target' => '_blank'), 'external' => TRUE));

      }
    }

    if (!empty($options)) {
      $form['explanation'] = array(
        '#markup' => t('This identity was used to create your account. Please choose another identity to replace it.'),
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      );
      $form['identity_choice'] = array(
        '#type' => 'radios',
        // '#title' => t('Identities'),
        '#options' => $options,
        '#default_value' => count($options) == 1 ? $key : NULL,
        // '#required' => TRUE, //required has bugs with radios http://drupal.org/node/811542.
      );
    }
    else {
      $form['explanation'] = array(
        '#markup' => t('This identity was used to create your account. To delete it you should first add another identity to your account.'),
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      );
      // Add more identities.
      if (\Drupal::currentUser()->hasPermission('use hybridauth')) {
        $form['hybridauth_widget'] = array(
          '#type' => 'hybridauth_widget',
          '#title' => t('Add more identities'),
          '#weight' => 10,
          '#hybridauth_widget_type' => 'list',
          '#hybridauth_destination' => '',
          '#hybridauth_destination_error' => '',
        );
      }
      return $form;
    }
  }

  $form = confirm_form($form, '', 'user/' . $account->uid . '/hybridauth');
  // @FIXME
// drupal_set_title() has been removed. There are now a few ways to set the title
// dynamically, depending on the situation.
// 
// 
// @see https://www.drupal.org/node/2067859
// drupal_set_title($username);


  return $form;
}

function hybridauth_user_identity_delete_validate($form, &$form_state) {
  if (!empty($form['identity_choice']) && empty($form_state['values']['identity_choice'])) {
    form_set_error('identity_choice', t('Please choose identity for replacement.'));
  }
}

function hybridauth_user_identity_delete_submit($form, &$form_state) {
  $account = $form['#user'];
  $del_identity = $form['#del_identity'];
  $del_identity_data = $form['#del_identity_data'];
  if (!empty($form_state['values']['identity_choice'])) {
    // Change hybridauth data used for tokens.
    $identity = _hybridauth_identity_load_by_id($form_state['values']['identity_choice']);
    $data = unserialize($identity['data']);
    $edit['data']['hybridauth'] = $data;
    // Change name.
    // $name = _hybridauth_make_username($data);
    // $edit['name'] = $name;
    // @FIXME
// user_save() is now a method of the user entity.
// $account = user_save($account, $edit);

  }

  $deleted = _hybridauth_identity_delete_by_id($del_identity['id']);
  if ($deleted) {
    drupal_set_message(t('Identity deleted.'));
    _hybridauth_invoke_hooks('hybridauth_identity_deleted', $account, $del_identity_data);
  }

  if ($hybridauth = hybridauth_get_instance()) {
    if (is_object($hybridauth)) {
      $adapter = $hybridauth->getAdapter($del_identity['provider']);
      $adapter->logout();
    }
  }

  $form_state['redirect'] = 'user/' . $account->uid . '/hybridauth';
}
